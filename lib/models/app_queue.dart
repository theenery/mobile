import 'dart:collection';

import 'package:bson/bson.dart';
import 'package:flutter/cupertino.dart';
import 'package:queuer/models/app_queue_member.dart';

class AppQueue with ChangeNotifier {
  bool? _allowRejoin;
  final ObjectId id;
  String _name;
  List<AppQueueMember> _sequence;

  AppQueue({
    bool? allowRejoin,
    required this.id,
    required String name,
    List<AppQueueMember>? sequence,
  })  : _allowRejoin = allowRejoin,
        _name = name,
        _sequence = sequence ?? [] {
    _recalculateOrder();
  }

  AppQueue.empty() : this(id: ObjectId.fromSeconds(0), name: '');

  AppQueue.fromJson(Map<String, dynamic> json)
      : _allowRejoin = json['allowRejoin'] as bool?,
        id = ObjectId.fromHexString(json['_id']),
        _name = json['name'] as String,
        _sequence = (json['sequence'] as List<dynamic>)
            .map((e) => AppQueueMember.fromJson(e))
            .toList() {
    _recalculateOrder();
  }

  bool? get allowRejoin => _allowRejoin;
  String get name => _name;
  List<AppQueueMember> get sequence => _sequence;

  AppQueueMember? get currentMember {
    return sequence
        .where((member) => member.isPrioritized)
        .followedBy(sequence.where((member) => member.isIncluded))
        .firstOrNull;
  }

  int get membersRemains => sequence
      .where((member) => member.isIncluded || member.isPrioritized)
      .length;

  AppQueueMember? get nextMember => sequence
      .where((member) => member.isPrioritized)
      .followedBy(sequence.where((member) => member.isIncluded))
      .skip(1)
      .firstOrNull;

  AppQueueMember? get previousMember {
    final filtered = sequence.where((member) => member.isExcluded);
    if (filtered.length < 2) return filtered.firstOrNull;
    return filtered
        .reduce((m1, m2) => m1.leaveMoment!.isAfter(m2.leaveMoment!) ? m1 : m2);
  }

  set allowRejoin(bool? value) {
    _allowRejoin = value;
    notifyListeners();
  }

  set name(String name) {
    _name = name;
    notifyListeners();
  }

  AppQueueMember findMemberById(ObjectId id) {
    return sequence.singleWhere((member) => member.id == id);
  }

  void memberErase(AppQueueMember member) {
    final modifiable = [..._sequence];
    modifiable.remove(member);
    _sequence = List.unmodifiable(modifiable);
    _recalculateOrder();
  }

  void memberJoin(AppQueueMember member) {
    final modifiable = [..._sequence, member];
    _sequence = List.unmodifiable(modifiable);
    _recalculateOrder();
  }

  void memberSetState(AppQueueMember member, AppMemberState state) {
    member.state = state;
    notifyListeners(); // previous, current, next can be changed
  }

  void memberShift(AppQueueMember member, int shift) {
    final index = sequence.indexOf(member);
    if (shift > 0) {
      for (int i = 0; i < shift && index + i < sequence.length; i++) {
        if (sequence[index + i + 1].shift > 0 &&
            sequence[index + i + 1].shift > member.shift) {
          sequence[index + i + 1].shift--;
        } else {
          member.shift++;
        }
      }
    } else {
      for (int i = 0; i > shift && index + i > 0; i--) {
        if (member.shift > 0 && member.shift > sequence[index + i - 1].shift) {
          member.shift--;
        } else {
          sequence[index + i - 1].shift++;
        }
      }
    }
    _recalculateOrder();
  }

  void _recalculateOrder() {
    /* sort by joinMoment and then for each member 
    shift it down by the value of the shift variable */
    final modifiable = _sequence.toList();
    modifiable.sort((m1, m2) => m1.joinMoment.compareTo(m2.joinMoment));
    for (int i = modifiable.length - 1; i >= 0; i--) {
      int shift = modifiable[i].shift;
      for (int j = 0; j < shift && i + j < modifiable.length - 1; j++) {
        final swap = modifiable[i + j];
        modifiable[i + j] = modifiable[i + j + 1];
        modifiable[i + j + 1] = swap;
      }
    }

    _sequence = List.unmodifiable(modifiable);
    notifyListeners();
  }
}
