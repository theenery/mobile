import 'dart:collection';
import 'package:flutter/cupertino.dart';
import 'package:queuer/models/app_mixed_queue_member.dart';
import 'package:queuer/models/app_queue.dart';

class AppMixedQueue with ChangeNotifier {
  final List<AppQueue> _queues;
  List<AppMixedQueueMember> _sequence = [];

  AppMixedQueue({List<AppQueue>? queues}) : _queues = queues ?? [] {
    for (var queue in _queues) {
      queue.addListener(_recalculateOrder);
    }
    _recalculateOrder();
  }

  @override
  void dispose() {
    for (var queue in _queues) {
      queue.removeListener(_recalculateOrder);
    }
    super.dispose();
  }

  AppMixedQueueMember? get currentMember {
    return sequence
        .where((m) => m.member.isPrioritized)
        .followedBy(sequence.where((m) => m.member.isIncluded))
        .firstOrNull;
  }

  int get membersRemains => sequence
      .where((m) => m.member.isIncluded || m.member.isPrioritized)
      .length;

  AppMixedQueueMember? get nextMember => sequence
      .where((m) => m.member.isPrioritized)
      .followedBy(sequence.where((m) => m.member.isIncluded))
      .skip(1)
      .firstOrNull;

  AppMixedQueueMember? get previousMember {
    final filtered = sequence.where((m) => m.member.isExcluded);
    if (filtered.length < 2) return filtered.firstOrNull;
    return filtered.reduce((m1, m2) =>
        m1.member.leaveMoment!.isAfter(m2.member.leaveMoment!) ? m1 : m2);
  }

  List<AppMixedQueueMember> get sequence => _sequence;

  void addQueue(AppQueue queue) {
    _queues.add(queue);
    queue.addListener(_recalculateOrder);
    _recalculateOrder();
  }

  void removeQueue(AppQueue queue) {
    _queues.remove(queue);
    queue.removeListener(_recalculateOrder);
    _recalculateOrder();
  }

  void _recalculateOrder() {
    final originalSequence = _queues
        .map((queue) => queue.sequence
            .map((member) => AppMixedQueueMember(queue: queue, member: member)))
        .fold(
            <AppMixedQueueMember>[],
            (previousValue, element) =>
                previousValue.followedBy(element).toList());

    originalSequence
        .sort((m1, m2) => m1.member.joinMoment.compareTo(m2.member.joinMoment));
    final sequence = <AppMixedQueueMember>[];
    for (int i = 0; i < originalSequence.length; i++) {
      final originalQueueSequence = originalSequence[i].queue.sequence.toList()
        ..sort((m1, m2) => m1.joinMoment.compareTo(m2.joinMoment));
      final indexInOriginal =
          originalQueueSequence.indexOf(originalSequence[i].member);
      final memberToAdd = originalSequence[i].queue.sequence[indexInOriginal];
      final mixedMember =
          originalSequence.singleWhere((mm) => mm.member.id == memberToAdd.id);
      sequence.add(mixedMember);
    }

    _sequence = List.unmodifiable(sequence);
    notifyListeners();
  }
}
