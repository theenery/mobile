import 'dart:collection';

import 'package:bson/bson.dart';
import 'package:flutter/cupertino.dart';
import 'package:queuer/models/app_group_member.dart';
import 'package:queuer/models/app_mixed_queue.dart';
import 'package:queuer/models/app_queue.dart';

class AppGroup with ChangeNotifier {
  bool _allowRejoin;
  final ObjectId id;
  final List<AppGroupMember> _members;
  late final AppMixedQueue mixedQueue;
  String _name;
  ObjectId _owner;
  List<AppQueue> _queues;

  AppGroup({
    bool allowRejoin = true,
    required this.id,
    List<AppGroupMember>? members,
    required String name,
    required ObjectId owner,
    List<AppQueue>? queues,
  })  : _allowRejoin = allowRejoin,
        _members = members ?? [],
        mixedQueue = AppMixedQueue(queues: queues?.toList()),
        _name = name,
        _owner = owner,
        _queues = List.unmodifiable(queues ?? []);

  AppGroup.empty()
      : this(
            id: ObjectId.fromSeconds(0),
            name: '',
            owner: ObjectId.fromSeconds(0));

  AppGroup.fromJson(Map<String, dynamic> json)
      : _allowRejoin = json['allowRejoin'] as bool,
        id = ObjectId.fromHexString(json['_id']),
        _members = (json['members'] as List<dynamic>)
            .map((e) => AppGroupMember.fromJson(e))
            .toList(),
        _name = json['name'] as String,
        _owner = ObjectId.fromHexString(json['owner']),
        _queues = List.unmodifiable(json['queues'] as List<dynamic>)
            .map((e) => AppQueue.fromJson(e))
            .toList() {
    mixedQueue = AppMixedQueue(queues: queues.toList());
  }

  bool get allowRejoin => _allowRejoin;
  String get joinLink => 'queuer.com/g/${id.toJson()}';
  UnmodifiableListView<AppGroupMember> get members =>
      UnmodifiableListView(_members);
  String get name => _name;
  ObjectId get owner => _owner;
  List<AppQueue> get queues => _queues;

  set allowRejoin(bool value) {
    _allowRejoin = value;
    notifyListeners();
  }

  set name(String value) {
    _name = value;
    notifyListeners();
  }

  set owner(ObjectId value) {
    _owner = value;
    notifyListeners();
  }

  void addMember(AppGroupMember member) {
    _members.add(member);
    notifyListeners();
  }

  void addQueue(AppQueue queue) {
    _queues = List.unmodifiable([..._queues, queue]);
    mixedQueue.addQueue(queue);
    notifyListeners();
  }

  AppGroupMember findMemberById(ObjectId id) {
    return _members.singleWhere((member) => member.user.id == id);
  }

  AppQueue findQueueById(ObjectId id) {
    return _queues.singleWhere((queue) => queue.id == id);
  }

  void removeMember(AppGroupMember member) {
    _members.remove(member);
    notifyListeners();
  }

  void removeQueue(AppQueue queue) {
    final modifiable = _queues.toList();
    modifiable.remove(queue);
    _queues = List.unmodifiable(modifiable);

    mixedQueue.removeQueue(queue);
    notifyListeners();
  }
}
