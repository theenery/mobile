import 'package:bson/bson.dart';
import 'package:flutter/material.dart';

class AppUser with ChangeNotifier {
  ObjectId _id;
  String _name;

  AppUser({required ObjectId id, required String name})
      : _id = id,
        _name = name;

  AppUser.empty()
      : _id = ObjectId.fromSeconds(0),
        _name = '';

  AppUser.fromJson(Map<String, dynamic> json)
      : _id = ObjectId.fromHexString(json['_id']),
        _name = json['name'] as String;

  ObjectId get id => _id;
  String get name => _name;

  set id(ObjectId value) {
    _id = value;
    notifyListeners();
  }

  set name(String value) {
    _name = value;
    notifyListeners();
  }
}
