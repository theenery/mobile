import 'package:bson/bson.dart';
import 'package:flutter/cupertino.dart';
import 'package:queuer/models/app_user.dart';

enum AppMemberState { included, excluded, skipped, prioritized }

class AppQueueMember with ChangeNotifier {
  final ObjectId id;
  final DateTime joinMoment;
  DateTime? _leaveMoment;
  int _shift;
  AppMemberState _state;
  final AppUser user;

  AppQueueMember({
    required this.id,
    required this.joinMoment,
    DateTime? leaveMoment,
    int shift = 0,
    required AppMemberState state,
    required this.user,
  })  : _shift = shift,
        _leaveMoment = leaveMoment,
        _state = state;

  AppQueueMember.fromJson(Map<String, dynamic> json)
      : id = ObjectId.fromHexString(json['_id']),
        joinMoment = DateTime.parse(json['joinMoment']),
        _leaveMoment = json['leaveMoment'] != null
            ? DateTime.parse(json['leaveMoment'])
            : null,
        _shift = json['shift'] as int? ?? 0,
        _state = AppMemberState.values.byName(json['state']),
        user = AppUser.fromJson(json['user']);

  bool get isExcluded => _state == AppMemberState.excluded;
  bool get isIncluded => _state == AppMemberState.included;
  bool get isSkipped => _state == AppMemberState.skipped;
  bool get isPrioritized => _state == AppMemberState.prioritized;
  DateTime? get leaveMoment => _leaveMoment;
  int get shift => _shift;
  AppMemberState get state => _state;

  set state(AppMemberState value) {
    _state = value;
    notifyListeners();
  }

  set leaveMoment(DateTime? value) {
    _leaveMoment = value;
    notifyListeners();
  }

  set shift(int value) {
    _shift = value;
    notifyListeners();
  }

  void exclude() {
    _state = AppMemberState.excluded;
    leaveMoment = DateTime.now();
    notifyListeners();
  }

  void include() {
    _state = AppMemberState.included;
    leaveMoment = null;
    notifyListeners();
  }

  void skip() {
    _state = AppMemberState.skipped;
    notifyListeners();
  }

  void prioritize() {
    _state = AppMemberState.prioritized;
    notifyListeners();
  }
}
