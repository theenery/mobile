import 'package:flutter/material.dart';
import 'package:queuer/models/app_user.dart';

class AppGroupMember with ChangeNotifier {
  bool _isModerator;
  bool? _notification;
  final AppUser user;

  AppGroupMember({
    bool isModerator = false,
    bool? notification,
    required this.user,
  })  : _isModerator = isModerator,
        _notification = notification;

  AppGroupMember.fromJson(Map<String, dynamic> json)
      : _isModerator = json['isModerator'] as bool,
        _notification = json['notification'] as bool?,
        user = AppUser.fromJson(json['user']);

  bool get isModerator => _isModerator;
  bool? get notification => _notification;

  set isModerator(bool value) {
    _isModerator = value;
    notifyListeners();
  }

  set notification(bool? value) {
    _notification = value;
    notifyListeners();
  }
}
