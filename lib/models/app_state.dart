import 'dart:collection';

import 'package:bson/bson.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:logger/logger.dart';
import 'package:queuer/models/app_group.dart';
import 'package:queuer/models/app_group_member.dart';
import 'package:queuer/models/app_queue.dart';
import 'package:queuer/models/app_queue_member.dart';
import 'package:queuer/models/app_user.dart';

import 'package:socket_io_client/socket_io_client.dart' as io;

class AppState with ChangeNotifier {
  AppGroup _currentGroup = AppGroup.empty();
  AppQueue _currentQueue = AppQueue.empty();
  final logger = Logger(printer: PrettyPrinter(lineLength: 60));
  List<AppGroup> _groups;
  late io.Socket socket;
  AppUser _user = AppUser.empty();

  bool notification = true;

  AppState({List<AppGroup>? groups, AppUser? user}) : _groups = groups ?? [] {
    if (user != null) _user = user;
  }

  @override
  void dispose() {
    disposeSocket();
    super.dispose();
  }

  AppGroup get currentGroup => _currentGroup;
  AppQueue get currentQueue => _currentQueue;
  UnmodifiableListView<AppGroup> get groups =>
      UnmodifiableListView<AppGroup>(_groups);
  AppUser get user => _user;

  set currentGroup(AppGroup group) {
    _currentGroup = group;
    notifyListeners();
  }

  set currentQueue(AppQueue queue) {
    _currentQueue = queue;
    notifyListeners();
  }

  void addGroup(AppGroup group) {
    _groups.add(group);
    notifyListeners();
  }

  void disposeSocket() {
    socket.dispose();
  }

  AppGroup findGroupById(ObjectId id) {
    return groups.singleWhere((group) => group.id == id);
  }

  void initSocket() {
    socket = io.io(
        'http://192.168.1.105:3000',
        io.OptionBuilder()
            .setTransports(['websocket'])
            .setExtraHeaders({'user': user.id.toJson()})
            .enableForceNew()
            .build());

    socket.onConnect((data) => logger.i('Connected'));
    socket.onDisconnect((data) => logger.i('Disconnected'));
    socket.on('oops', (data) => logger.e(data));

    socket.on('addGroupMemberResponse', (data) {
      try {
        final group = AppGroup.fromJson(data['group']);
        if (groups.any((g) => g.id == group.id)) {
          groups
              .singleWhere((g) => g.id == group.id)
              .addMember(AppGroupMember.fromJson(data['groupMember']));
        } else {
          addGroup(group);
        }
      } catch (e) {
        logger.e(e);
      }
    });
    socket.on(
      'createGroupResponse',
      (data) {
        try {
          addGroup(AppGroup.fromJson(data['group']));
        } catch (e) {
          logger.e(e);
        }
      },
    );

    socket.on(
      'createQueueResponse',
      (data) {
        try {
          findGroupById(ObjectId.fromHexString(data['group']))
              .addQueue(AppQueue.fromJson(data['queue']));
        } catch (e) {
          logger.e(e);
        }
      },
    );

    socket.on('deleteGroupResponse', (data) {
      try {
        removeGroup(groups.singleWhere(
            (group) => group.id == ObjectId.fromHexString(data['group'])));
      } catch (e) {
        logger.e(e);
      }
    });

    socket.on('deleteGroupMemberResponse', (data) {
      try {
        final groupMember = ObjectId.fromHexString(data['groupMember']);
        final group = findGroupById(ObjectId.fromHexString(data['group']));
        if (groupMember == user.id) {
          removeGroup(group);
        } else {
          group.removeMember(group.findMemberById(groupMember));
        }
      } catch (e) {
        logger.e(e);
      }
    });

    socket.on('deleteQueueResponse', (data) {
      try {
        final group = findGroupById(ObjectId.fromHexString(data['group']));
        final queue = group.queues.singleWhere(
            (queue) => queue.id == ObjectId.fromHexString(data['queue']));
        group.removeQueue(queue);
      } catch (e) {
        logger.e(e);
      }
    });

    socket.on('fetchUserDataResponse', (data) {
      _groups = (data['groups'] as List<dynamic>)
          .map((e) => AppGroup.fromJson(e))
          .toList();
      _user = (AppUser.fromJson(data['user']));
      notifyListeners();
    });

    socket.on('memberEraseResponse', (data) {
      try {
        final group = findGroupById(ObjectId.fromHexString(data['group']));
        final queue =
            group.findQueueById(ObjectId.fromHexString(data['queue']));
        queue.memberErase(
            queue.findMemberById(ObjectId.fromHexString(data['queueMember'])));
      } catch (e) {
        logger.e(e);
      }
    });

    socket.on('memberJoinResponse', (data) {
      try {
        final group = findGroupById(ObjectId.fromHexString(data['group']));
        final queue =
            group.findQueueById(ObjectId.fromHexString(data['queue']));
        queue.memberJoin(AppQueueMember.fromJson(data['queueMember']));
      } catch (e) {
        logger.e(e);
      }
    });

    socket.on('memberSetStateResponse', (data) {
      try {
        final group = findGroupById(ObjectId.fromHexString(data['group']));
        final queue =
            group.findQueueById(ObjectId.fromHexString(data['queue']));
        final member =
            queue.findMemberById(ObjectId.fromHexString(data['queueMember']));

        if (data['leaveMoment'] != null) {
          member.leaveMoment = DateTime.parse(data['leaveMoment']);
        }
        queue.memberSetState(
            member, AppMemberState.values.byName(data['state']));
      } catch (e) {
        logger.e(e);
      }
    });

    socket.on('memberShiftResponse', (data) {
      try {
        final group = findGroupById(ObjectId.fromHexString(data['group']));
        final queue =
            group.findQueueById(ObjectId.fromHexString(data['queue']));
        final member =
            queue.findMemberById(ObjectId.fromHexString(data['queueMember']));
        queue.memberShift(member, data['shift']);
      } catch (e) {
        logger.e(e);
      }
    });

    socket.on('updateGroupResponse', (data) {
      try {
        final group = findGroupById(ObjectId.fromHexString(data['group']));
        group.allowRejoin = data['allowRejoin'] as bool;
        group.name = data['name'];
      } catch (e) {
        logger.e(e);
      }
    });

    socket.on('updateGroupNotificationResponse', (data) {
      try {
        final group = findGroupById(ObjectId.fromHexString(data['group']));
        group.members
            .singleWhere(
                (m) => m.user.id == ObjectId.fromHexString(data['user']))
            .notification = data['notification'] as bool?;
      } catch (e) {
        logger.e(e);
      }
    });

    socket.on('updateModeratorResponse', (data) {
      try {
        final group = findGroupById(ObjectId.fromHexString(data['group']));
        group.members
            .singleWhere(
                (m) => m.user.id == ObjectId.fromHexString(data['groupMember']))
            .isModerator = data['isModerator'] as bool;
      } catch (e) {
        logger.e(e);
      }
    });

    socket.on('updateQueueResponse', (data) {
      try {
        final group = findGroupById(ObjectId.fromHexString(data['group']));
        final queue =
            group.findQueueById(ObjectId.fromHexString(data['queue']));
        queue.allowRejoin = data['allowRejoin'] as bool?;
        queue.name = data['name'];
      } catch (e) {
        logger.e(e);
      }
    });

    socket.on('updateUserNameResponse', (data) {
      try {
        user.name = data['name'];
      } catch (e) {
        logger.e(e);
      }
    });
  }

  void removeGroup(AppGroup group) {
    _groups.remove(group);
    notifyListeners();
  }
}
