import 'package:queuer/models/app_queue.dart';
import 'package:queuer/models/app_queue_member.dart';

class AppMixedQueueMember {
  final AppQueueMember member;
  final AppQueue queue;

  AppMixedQueueMember({required this.member, required this.queue});
}
