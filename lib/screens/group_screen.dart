import 'package:bson/bson.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:queuer/models/app_group.dart';
import 'package:queuer/models/app_queue.dart';
import 'package:queuer/models/app_state.dart';
import 'package:queuer/models/app_user.dart';
import 'package:queuer/screens/create_queue_screen.dart';
import 'package:queuer/screens/group_settings_screen.dart';
import 'package:queuer/screens/mixed_queue_screen.dart';
import 'package:queuer/screens/queue_screen.dart';

class GroupScreen extends StatelessWidget {
  const GroupScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Selector<AppGroup, String>(
          selector: (context, group) => group.name,
          builder: (context, name, child) => Text(name),
        ),
        actions: [
          IconButton(
            icon: const Icon(Icons.settings),
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => const GroupSettingsScreen()));
            },
          )
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Selector<AppGroup, List<AppQueue>>(
          selector: (context, group) => group.queues,
          shouldRebuild: (previous, next) => previous.length != next.length,
          builder: (context, queues, child) => ListView(
            children: [
              if (queues.isEmpty)
                const Padding(
                  padding: EdgeInsets.all(24.0),
                  child: Text(
                    'There are no queues yet',
                    textAlign: TextAlign.center,
                  ),
                ),
              if (queues.length > 1)
                ListTile(
                  leading: const Icon(Icons.push_pin),
                  title: const Text('Mixed queue'),
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const MixedQueueScreen()));
                  },
                ),
              for (AppQueue queue in queues)
                ChangeNotifierProvider<AppQueue>.value(
                  value: queue,
                  child: Selector<AppQueue, String>(
                    selector: (context, queue) => queue.name,
                    builder: (context, name, child) => ListTile(
                      leading: const Icon(Icons.group),
                      title: Text(name),
                      onTap: () {
                        context.read<AppState>().currentQueue = queue;
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => const QueueScreen()));
                      },
                    ),
                  ),
                ),
            ],
          ),
        ),
      ),
      floatingActionButton: Selector<AppUser, ObjectId>(
        selector: (context, user) => user.id,
        builder: (context, user, child) => Selector<AppGroup, ObjectId>(
          selector: (context, group) => group.owner,
          builder: (context, owner, child) => (user == owner)
              ? ElevatedButton(
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: ((context) => const CreateQueueScreen())));
                  },
                  child: const Padding(
                    padding: EdgeInsets.symmetric(vertical: 8.0),
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Icon(Icons.add),
                        Text('Create'),
                      ],
                    ),
                  ),
                )
              : const SizedBox.shrink(),
        ),
      ),
    );
  }
}
