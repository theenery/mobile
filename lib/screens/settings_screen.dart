import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:queuer/models/app_state.dart';
import 'package:queuer/models/app_user.dart';
import 'package:queuer/themes/app_color_palette.dart';
import 'package:queuer/themes/app_color_sheme.dart';

class SettingsScreen extends StatefulWidget {
  const SettingsScreen({super.key});

  @override
  State<SettingsScreen> createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  final _formKey = GlobalKey<FormState>();
  late TextEditingController _nameController;
  late bool _notificationOption;
  final _notificationOptions = {'Yes': true, 'No': false};
  late ThemeMode _themeOption;
  final _themeOptions = {
    'System': ThemeMode.system,
    'Dark': ThemeMode.dark,
    'Light': ThemeMode.light,
  };

  @override
  void initState() {
    super.initState();

    _nameController = TextEditingController(text: context.read<AppUser>().name);
    _notificationOption = context.read<AppState>().notification;
    _themeOption = context.read<AppColorScheme>().themeMode;
  }

  @override
  void dispose() {
    _nameController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Settings'),
        actions: [
          IconButton(
            onPressed: () {
              if (!_formKey.currentState!.validate()) return;

              context.read<AppState>().socket.emit(
                  'updateUserName', {'name': _nameController.text.trim()});
              //.notification = _notificationOption;
              context.read<AppColorScheme>().themeMode = _themeOption;
              Navigator.pop(context);
            },
            icon: const Icon(Icons.done),
          ),
        ],
      ),
      body: Form(
        key: _formKey,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 24.0),
          child: Column(
            children: [
              TextFormField(
                decoration: const InputDecoration(
                  label: Text('Name'),
                  contentPadding: EdgeInsets.all(8.0),
                ),
                controller: _nameController,
                validator: (value) {
                  if (value == null || value.trim().isEmpty) {
                    return 'Please enter the name';
                  }
                  return null;
                },
              ),
              const SizedBox(height: 12.0),
              DropdownButtonFormField(
                decoration: const InputDecoration(
                  label: Text('Notify me when it`s my turn'),
                  contentPadding: EdgeInsets.all(8.0),
                ),
                value: _notificationOption,
                items: _notificationOptions.entries
                    .map(
                      (entry) => DropdownMenuItem(
                        value: entry.value,
                        child: Text(entry.key),
                      ),
                    )
                    .toList(),
                onChanged: (option) => _notificationOption = option!,
              ),
              const SizedBox(height: 12.0),
              DropdownButtonFormField(
                  decoration: const InputDecoration(
                    label: Text('Theme'),
                    contentPadding: EdgeInsets.all(8.0),
                  ),
                  value: _themeOption,
                  items: _themeOptions.entries
                      .map(
                        (entry) => DropdownMenuItem(
                          value: entry.value,
                          child: Text(entry.key),
                        ),
                      )
                      .toList(),
                  onChanged: (option) => _themeOption = option!),
              const SizedBox(height: 12.0),
              TextButton(
                style: TextButton.styleFrom(
                  shape: const BeveledRectangleBorder(),
                  padding: const EdgeInsets.symmetric(
                      horizontal: 8.0, vertical: 16.0),
                ),
                child: const SizedBox(
                  width: double.infinity,
                  child: Text('Color palette'),
                ),
                onPressed: () {
                  showModalBottomSheet(
                    context: context,
                    isScrollControlled: true,
                    builder: (context) => Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: SingleChildScrollView(
                        child: Column(
                          children: [
                            GridView.count(
                              shrinkWrap: true,
                              childAspectRatio: 3.0,
                              crossAxisCount: 2,
                              children: [
                                ...AppColorPalette.palettes.map(
                                  (palette) => PalettePicker(palette: palette),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class PalettePicker extends StatelessWidget {
  final AppColorPalette palette;

  const PalettePicker({super.key, required this.palette});

  @override
  Widget build(BuildContext context) {
    return MouseRegion(
      cursor: SystemMouseCursors.click,
      child: GestureDetector(
        onTap: () {
          context.read<AppColorScheme>().colorPalette = palette;
          Navigator.pop(context);
        },
        child: Container(
          margin: const EdgeInsets.all(8.0),
          clipBehavior: Clip.antiAlias,
          decoration: BoxDecoration(borderRadius: BorderRadius.circular(8.0)),
          child: CustomPaint(
            size: Size.infinite,
            painter: DiagonalDividedRectPainter(
              palette.accent,
              palette.accentVariant,
            ),
          ),
        ),
      ),
    );
  }
}

class DiagonalDividedRectPainter extends CustomPainter {
  final Color ulColor;
  final Color brColor;

  const DiagonalDividedRectPainter(this.ulColor, this.brColor);

  @override
  void paint(Canvas canvas, Size size) {
    Paint ulPaint = Paint()..color = ulColor;
    Paint brPaint = Paint()..color = brColor;

    canvas.drawPath(
        Path()
          ..moveTo(0.0, 0.0)
          ..lineTo(0.0, size.height)
          ..lineTo(size.width, 0.0)
          ..close(),
        ulPaint);
    canvas.drawPath(
        Path()
          ..moveTo(size.width, size.height)
          ..lineTo(0.0, size.height)
          ..lineTo(size.width, 0.0)
          ..close(),
        brPaint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}
