import 'package:bson/bson.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:queuer/models/app_group.dart';
import 'package:queuer/models/app_queue.dart';
import 'package:queuer/models/app_state.dart';
import 'package:queuer/models/app_user.dart';

class JoinMemberScreen extends StatelessWidget {
  const JoinMemberScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Join member'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Consumer<AppGroup>(
          builder: (context, group, child) => Consumer<AppQueue>(
            builder: (context, queue, child) {
              final allowRejoin = queue.allowRejoin ?? group.allowRejoin;
              final membersInQueue = queue.sequence
                  .map((queueMember) => queueMember.user.id)
                  .toSet();
              final user = context.select<AppUser, ObjectId>((user) => user.id);

              return ListView.builder(
                itemCount: group.members.length,
                itemBuilder: (context, index) {
                  final member = group.members[index];
                  if (member.user.id == user) {
                    return const SizedBox.shrink();
                  }

                  final enabled =
                      allowRejoin || !membersInQueue.contains(member.user.id);
                  return ListTile(
                    enabled: enabled,
                    title: Text(member.user.name),
                    leading: const Icon(Icons.person),
                    trailing: const Icon(Icons.add),
                    onTap: () {
                      context.read<AppState>().socket.emit('memberJoin', {
                        'group': group.id,
                        'groupMember': member.user.id,
                        'queue': queue.id
                      });
                      Navigator.pop(context);
                    },
                  );
                },
              );
            },
          ),
        ),
      ),
    );
  }
}
