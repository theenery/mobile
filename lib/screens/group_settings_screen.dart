import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:queuer/models/app_group.dart';
import 'package:queuer/models/app_state.dart';
import 'package:queuer/models/app_user.dart';
import 'package:queuer/screens/group_members_screen.dart';

class GroupSettingsScreen extends StatefulWidget {
  const GroupSettingsScreen({super.key});

  @override
  State<GroupSettingsScreen> createState() => _GroupSettingsScreenState();
}

class _GroupSettingsScreenState extends State<GroupSettingsScreen> {
  late bool _allowRejoinOption;
  final _allowRejoinOptions = {'Yes': true, 'No': false};
  final _formKey = GlobalKey<FormState>();
  late final String _joinLink;
  late TextEditingController _nameController;
  late bool? _notificationOption;
  final _notificationOptions = {'Default': null, 'Yes': true, 'No': false};

  @override
  void initState() {
    super.initState();

    _allowRejoinOption = context.read<AppGroup>().allowRejoin;
    _joinLink = context.read<AppGroup>().joinLink;
    _nameController =
        TextEditingController(text: context.read<AppGroup>().name);
    _notificationOption = context
        .read<AppGroup>()
        .members
        .singleWhere((member) => member.user.id == context.read<AppUser>().id)
        .notification;
  }

  @override
  void dispose() {
    _nameController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final user = context.read<AppUser>().id;
    final userIsOwner = user == context.read<AppGroup>().owner;

    return Scaffold(
      appBar: AppBar(
        title: Selector<AppGroup, String>(
            selector: (context, group) => group.name,
            builder: (context, name, child) => Text(name)),
        actions: [
          IconButton(
            onPressed: () {
              if (!_formKey.currentState!.validate()) return;

              final allowRejoinChanged =
                  _allowRejoinOption != context.read<AppGroup>().allowRejoin;
              final nameChanged =
                  _nameController.text.trim() != context.read<AppGroup>().name;
              final notificationChanged = _notificationOption !=
                  context
                      .read<AppGroup>()
                      .members
                      .singleWhere((m) => m.user.id == user)
                      .notification;

              if (userIsOwner && (allowRejoinChanged || nameChanged)) {
                context.read<AppState>().socket.emit('updateGroup', {
                  'group': context.read<AppGroup>().id,
                  'allowRejoin': _allowRejoinOption,
                  'name': _nameController.text.trim()
                });
              }

              if (notificationChanged) {
                context.read<AppState>().socket.emit(
                    'updateGroupNotification', {
                  'group': context.read<AppGroup>().id,
                  'notification': _notificationOption
                });
              }

              Navigator.pop(context);
            },
            icon: const Icon(Icons.done),
          ),
        ],
      ),
      body: Form(
        key: _formKey,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 24.0),
          child: Column(
            children: [
              if (userIsOwner)
                TextFormField(
                  decoration: const InputDecoration(
                    label: Text('Name'),
                    contentPadding: EdgeInsets.all(8.0),
                  ),
                  controller: _nameController,
                  validator: (value) {
                    if (value == null || value.trim().isEmpty) {
                      return 'Please enter the name';
                    }
                    return null;
                  },
                ),
              const SizedBox(height: 12.0),
              TextFormField(
                readOnly: true,
                initialValue: _joinLink,
                decoration: const InputDecoration(
                  label: Text('Link'),
                  contentPadding: EdgeInsets.all(8.0),
                ),
                onTap: () {
                  Clipboard.setData(ClipboardData(text: _joinLink));
                  ScaffoldMessenger.of(context).showSnackBar(
                    const SnackBar(
                      content: Text('Copied!'),
                    ),
                  );
                },
              ),
              const SizedBox(height: 12.0),
              if (userIsOwner)
                DropdownButtonFormField(
                    decoration: const InputDecoration(
                      label: Text('Allow rejoining queue'),
                      contentPadding: EdgeInsets.all(8.0),
                    ),
                    value: _allowRejoinOption,
                    items: _allowRejoinOptions.entries
                        .map(
                          (entry) => DropdownMenuItem(
                            value: entry.value,
                            child: Text(entry.key),
                          ),
                        )
                        .toList(),
                    onChanged: (option) => _allowRejoinOption = option!),
              const SizedBox(height: 12.0),
              DropdownButtonFormField(
                decoration: const InputDecoration(
                  label: Text('Notify me when it`s my turn'),
                  contentPadding: EdgeInsets.all(8.0),
                ),
                value: _notificationOption,
                items: _notificationOptions.entries
                    .map(
                      (entry) => DropdownMenuItem(
                        value: entry.value,
                        child: Text(entry.key),
                      ),
                    )
                    .toList(),
                onChanged: (option) => _notificationOption = option,
              ),
              const SizedBox(height: 24.0),
              TextButton(
                style: TextButton.styleFrom(
                  shape: const BeveledRectangleBorder(),
                  padding: const EdgeInsets.symmetric(
                      horizontal: 8.0, vertical: 16.0),
                ),
                child: const SizedBox(
                  width: double.infinity,
                  child: Text('Members'),
                ),
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const GroupMembersScreen()));
                },
              ),
              const SizedBox(height: 12.0),
              TextButton(
                style: TextButton.styleFrom(
                  shape: const BeveledRectangleBorder(),
                  padding: const EdgeInsets.symmetric(
                      horizontal: 8.0, vertical: 16.0),
                  foregroundColor: Theme.of(context).colorScheme.error,
                ),
                child: const SizedBox(
                  width: double.infinity,
                  child: Text('Leave this group'),
                ),
                onPressed: () {
                  showDialog(
                    context: context,
                    builder: (context) => AlertDialog(
                      title: const Text('Leave group'),
                      content: const SingleChildScrollView(
                        child: Text(
                          'Are you sure you want to leave this group?',
                        ),
                      ),
                      actions: [
                        TextButton(
                          style: TextButton.styleFrom(
                              foregroundColor:
                                  Theme.of(context).colorScheme.error),
                          onPressed: () {
                            context
                                .read<AppState>()
                                .socket
                                .emit('deleteGroupMember', {
                              'group': context.read<AppGroup>().id,
                              'groupMember': context.read<AppUser>().id,
                            });

                            Navigator.pop(context);
                            Navigator.pop(context);
                            Navigator.pop(context);
                          },
                          child: const Text(
                            'Leave',
                            textScaler: TextScaler.linear(1.25),
                          ),
                        ),
                        TextButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          child: const Text(
                            'Cancel',
                            textScaler: TextScaler.linear(1.25),
                          ),
                        ),
                      ],
                    ),
                  );
                },
              ),
              const SizedBox(height: 12.0),
              if (userIsOwner)
                TextButton(
                  style: TextButton.styleFrom(
                    shape: const BeveledRectangleBorder(),
                    padding: const EdgeInsets.symmetric(
                        horizontal: 8.0, vertical: 16.0),
                    foregroundColor: Theme.of(context).colorScheme.error,
                  ),
                  child: const SizedBox(
                    width: double.infinity,
                    child: Text('Delete this group'),
                  ),
                  onPressed: () {
                    showDialog(
                      context: context,
                      builder: (context) => AlertDialog(
                        title: const Text('Delete group'),
                        content: const SingleChildScrollView(
                          child: Text(
                            'Are you sure you want to delete this group forever?',
                          ),
                        ),
                        actions: [
                          TextButton(
                            style: TextButton.styleFrom(
                                foregroundColor:
                                    Theme.of(context).colorScheme.error),
                            onPressed: () {
                              context
                                  .read<AppState>()
                                  .socket
                                  .emit('deleteGroup', {
                                'group': context.read<AppGroup>().id,
                              });

                              Navigator.pop(context);
                              Navigator.pop(context);
                              Navigator.pop(context);
                            },
                            child: const Text(
                              'Delete',
                              textScaler: TextScaler.linear(1.25),
                            ),
                          ),
                          TextButton(
                            onPressed: () {
                              Navigator.pop(context);
                            },
                            child: const Text(
                              'Cancel',
                              textScaler: TextScaler.linear(1.25),
                            ),
                          ),
                        ],
                      ),
                    );
                  },
                ),
            ],
          ),
        ),
      ),
    );
  }
}
