import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:queuer/models/app_group.dart';
import 'package:queuer/models/app_queue.dart';
import 'package:queuer/models/app_state.dart';
import 'package:queuer/models/app_user.dart';

class QueueSettingsScreen extends StatefulWidget {
  const QueueSettingsScreen({super.key});

  @override
  State<QueueSettingsScreen> createState() => _QueueSettingsScreenState();
}

class _QueueSettingsScreenState extends State<QueueSettingsScreen> {
  late bool? _allowRejoinOption;
  final _allowRejoinOptions = {'Default': null, 'Yes': true, 'No': false};
  final _formKey = GlobalKey<FormState>();
  late TextEditingController _nameController;

  @override
  void initState() {
    super.initState();

    _allowRejoinOption = context.read<AppQueue>().allowRejoin;
    _nameController =
        TextEditingController(text: context.read<AppQueue>().name);
  }

  @override
  void dispose() {
    _nameController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Selector<AppQueue, String>(
            selector: (context, queue) => queue.name,
            builder: (context, name, child) => Text(name)),
        actions: [
          IconButton(
            onPressed: () {
              if (!_formKey.currentState!.validate()) return;

              final userIsOwner =
                  context.read<AppGroup>().owner == context.read<AppUser>().id;

              final allowRejoinChanged =
                  context.read<AppQueue>().allowRejoin != _allowRejoinOption;
              final nameChanged =
                  context.read<AppQueue>().name != _nameController.text.trim();

              if (!userIsOwner || !(allowRejoinChanged || nameChanged)) return;

              context.read<AppState>().socket.emit('updateQueue', {
                'group': context.read<AppGroup>().id,
                'queue': context.read<AppQueue>().id,
                'allowRejoin': _allowRejoinOption,
                'name': _nameController.text.trim(),
              });

              Navigator.pop(context);
            },
            icon: const Icon(Icons.done),
          ),
        ],
      ),
      body: Form(
        key: _formKey,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 24.0),
          child: Column(
            children: [
              TextFormField(
                decoration: const InputDecoration(
                  label: Text('Name'),
                  contentPadding: EdgeInsets.all(8.0),
                ),
                controller: _nameController,
                validator: (value) {
                  if (value == null || value.trim().isEmpty) {
                    return 'Please enter the name';
                  }
                  return null;
                },
              ),
              const SizedBox(height: 12.0),
              DropdownButtonFormField(
                  decoration: const InputDecoration(
                    label: Text('Allow rejoining queue'),
                    contentPadding: EdgeInsets.all(8.0),
                  ),
                  value: _allowRejoinOption,
                  items: _allowRejoinOptions.entries
                      .map(
                        (entry) => DropdownMenuItem(
                          value: entry.value,
                          child: Text(entry.key),
                        ),
                      )
                      .toList(),
                  onChanged: (option) => _allowRejoinOption = option),
              const SizedBox(height: 24.0),
              TextButton(
                style: TextButton.styleFrom(
                    shape: const BeveledRectangleBorder(),
                    padding: const EdgeInsets.symmetric(
                        horizontal: 8.0, vertical: 16.0),
                    foregroundColor: Theme.of(context).colorScheme.error),
                onPressed: () {
                  showDialog(
                    context: context,
                    builder: (context) => AlertDialog(
                      title: const Text('Delete queue'),
                      content: const SingleChildScrollView(
                        child: Text(
                          'Are you sure you want to delete this queue forever?',
                        ),
                      ),
                      actions: [
                        TextButton(
                          style: TextButton.styleFrom(
                              foregroundColor:
                                  Theme.of(context).colorScheme.error),
                          onPressed: () {
                            context.read<AppState>().socket.emit(
                                'deleteQueue', {
                              'group': context.read<AppGroup>().id,
                              'queue': context.read<AppQueue>().id
                            });

                            Navigator.pop(context);
                            Navigator.pop(context);
                            Navigator.pop(context);
                          },
                          child: const Text(
                            'Delete',
                            textScaler: TextScaler.linear(1.25),
                          ),
                        ),
                        TextButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          child: const Text(
                            'Cancel',
                            textScaler: TextScaler.linear(1.25),
                          ),
                        ),
                      ],
                    ),
                  );
                },
                child: const SizedBox(
                  width: double.infinity,
                  child: Text('Delete this queue'),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
