import 'package:bson/bson.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:queuer/models/app_group.dart';
import 'package:queuer/models/app_group_member.dart';
import 'package:queuer/models/app_queue.dart';
import 'package:queuer/models/app_queue_member.dart';
import 'package:queuer/models/app_state.dart';
import 'package:queuer/models/app_user.dart';
import 'package:queuer/screens/join_member_screen.dart';
import 'package:queuer/screens/queue_settings_screen.dart';

class QueueScreen extends StatelessWidget {
  const QueueScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final user = context.select<AppUser, ObjectId>((user) => user.id);

    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          title: Selector<AppQueue, String>(
              selector: (context, queue) => queue.name,
              builder: (context, name, child) => Text(name)),
          actions: [
            if (context.select<AppGroup, ObjectId>((group) => group.owner) ==
                user)
              IconButton(
                icon: const Icon(Icons.settings),
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const QueueSettingsScreen()));
                },
              )
          ],
          bottom: TabBar(
            unselectedLabelColor: Theme.of(context).colorScheme.primary,
            tabs: const [Tab(text: 'All'), Tab(text: 'Brief')],
            indicatorSize: TabBarIndicatorSize.tab,
            labelPadding: const EdgeInsets.all(0.0),
          ),
        ),
        body: TabBarView(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Selector<AppQueue, List<AppQueueMember>>(
                selector: (context, queue) => queue.sequence,
                builder: (context, sequence, child) {
                  if (sequence.isEmpty) {
                    return const Padding(
                      padding: EdgeInsets.all(24.0),
                      child: Text(
                        'There are no members yet',
                        textAlign: TextAlign.center,
                      ),
                    );
                  } else {
                    final group =
                        context.select<AppGroup, ObjectId>((group) => group.id);
                    final queue = context.select<AppState, AppQueue>(
                        (state) => state.currentQueue);
                    final user =
                        context.select<AppUser, ObjectId>((user) => user.id);
                    final members =
                        context.select<AppGroup, List<AppGroupMember>>(
                            (group) => group.members);
                    final userIsModerator = members
                        .singleWhere((m) => m.user.id == user)
                        .isModerator;

                    return ReorderableListView.builder(
                      onReorderStart: (index) {},
                      onReorder: (oldIndex, newIndex) {
                        var shift = newIndex - oldIndex;
                        if (shift > 0) shift--;

                        if (!userIsModerator &&
                            sequence[oldIndex].user.id != user) {
                          ScaffoldMessenger.of(context).showSnackBar(
                            const SnackBar(
                              content: Text(
                                  'You haven`t rights to shift other members'),
                            ),
                          );
                          return;
                        }

                        if (!userIsModerator && shift < 0) {
                          ScaffoldMessenger.of(context).showSnackBar(
                            const SnackBar(
                              content: Text(
                                  'You haven`t rights to shift yourself up'),
                            ),
                          );
                          return;
                        }

                        queue.memberShift(sequence[oldIndex], shift);

                        context.read<AppState>().socket.emit('memberShift', {
                          'group': group,
                          'queue': queue.id,
                          'queueMember': sequence[oldIndex].id,
                          'shift': shift,
                        });
                      },
                      itemCount: sequence.length,
                      itemBuilder: (context, index) {
                        return MemberTile(
                          key: Key('$index'),
                          context: context,
                          member: sequence[index],
                          queue: queue,
                          showShift: false,
                        );
                      },
                    );
                  }
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Selector<
                  AppQueue,
                  ({
                    AppQueueMember? previous,
                    AppQueueMember? current,
                    AppQueueMember? next,
                    int remains,
                  })>(
                selector: (context, queue) => (
                  previous: queue.previousMember,
                  current: queue.currentMember,
                  next: queue.nextMember,
                  remains: queue.membersRemains,
                ),
                builder: (context, queueData, child) {
                  final queueRef = context.select<AppState, AppQueue>(
                      (state) => state.currentQueue);

                  return Column(
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text(
                            'Previous',
                            textScaler: TextScaler.linear(1.25),
                          ),
                          MemberTile(
                            context: context,
                            member: queueData.previous,
                            queue: queueRef,
                            showShift: false,
                          ),
                        ],
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text(
                            'Current',
                            textScaler: TextScaler.linear(1.25),
                          ),
                          MemberTile(
                            context: context,
                            member: queueData.current,
                            queue: queueRef,
                            showShift: false,
                          ),
                        ],
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text(
                            'Next',
                            textScaler: TextScaler.linear(1.25),
                          ),
                          MemberTile(
                            context: context,
                            member: queueData.next,
                            queue: queueRef,
                            showShift: false,
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          Text(
                            'Remains ${queueData.remains}',
                            textScaler: const TextScaler.linear(1.25),
                          ),
                          Icon(
                            Icons.person,
                            color: Theme.of(context).colorScheme.surfaceVariant,
                          ),
                        ],
                      ),
                    ],
                  );
                },
              ),
            ),
          ],
        ),
        floatingActionButton: Consumer<AppGroup>(
          builder: (context, group, child) => Consumer<AppQueue>(
            builder: (context, queue, child) =>
                ChangeNotifierProvider<AppGroupMember>.value(
              value: group.members
                  .singleWhere((m) => m.user.id == context.read<AppUser>().id),
              child: Consumer<AppGroupMember>(
                builder: (context, userAsMember, child) {
                  final allowRejoin = queue.allowRejoin ?? group.allowRejoin;
                  final isUserInQueue =
                      queue.sequence.any((member) => member.user.id == user);

                  final canJoin = allowRejoin ? true : !isUserInQueue;
                  final canJoinMember = userAsMember.isModerator;

                  return (canJoin || canJoinMember)
                      ? ElevatedButton(
                          onPressed: () {
                            showModalBottomSheet(
                              isScrollControlled: true,
                              context: context,
                              builder: (context) => SingleChildScrollView(
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Column(
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      const Text(
                                        'Action',
                                        textAlign: TextAlign.center,
                                        textScaler: TextScaler.linear(2.0),
                                      ),
                                      if (canJoin)
                                        ListTile(
                                          leading: const Icon(Icons.add),
                                          title: const Text('Join'),
                                          onTap: () {
                                            context
                                                .read<AppState>()
                                                .socket
                                                .emit('memberJoin', {
                                              'group': group.id,
                                              'groupMember': user,
                                              'queue': queue.id
                                            });
                                            Navigator.pop(context);
                                          },
                                        ),
                                      if (canJoinMember)
                                        ListTile(
                                          leading: const Icon(Icons.person_add),
                                          title: const Text('Join member'),
                                          onTap: () {
                                            Navigator.pop(context);
                                            Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        const JoinMemberScreen()));
                                          },
                                        ),
                                    ],
                                  ),
                                ),
                              ),
                            );
                          },
                          child: const Padding(
                            padding: EdgeInsets.symmetric(vertical: 8.0),
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Icon(Icons.menu),
                                SizedBox(width: 8.0),
                                Text('Action'),
                              ],
                            ),
                          ),
                        )
                      : const SizedBox.shrink();
                },
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class MemberTileTapHandler extends StatelessWidget {
  final Widget Function(BuildContext, void Function()) builder;
  final AppQueueMember member;
  final AppQueue queue;

  const MemberTileTapHandler({
    super.key,
    required this.builder,
    required this.member,
    required this.queue,
  });

  @override
  Widget build(BuildContext context) {
    final group = context.select<AppGroup, ObjectId>((group) => group.id);
    final user = context.select<AppUser, ObjectId>((user) => user.id);
    final members = context
        .select<AppGroup, List<AppGroupMember>>((group) => group.members);

    onTap() {
      final userIsModerator =
          members.singleWhere((m) => m.user.id == user).isModerator;
      final userIsMember = member.user.id == user;

      if (!userIsMember && !userIsModerator) {
        return;
      }

      emitSetState(AppMemberState memberState) {
        context.read<AppState>().socket.emit('memberSetState', {
          'group': group,
          'queue': queue.id,
          'queueMember': member.id,
          'state': memberState.name,
        });
      }

      showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        builder: (context) => SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                const Text(
                  'Action',
                  textAlign: TextAlign.center,
                  textScaler: TextScaler.linear(2.0),
                ),
                if (member.isIncluded || member.isPrioritized)
                  ListTile(
                    leading: const Icon(Icons.timer),
                    title: const Text('Skip'),
                    onTap: () {
                      emitSetState(AppMemberState.skipped);
                      Navigator.pop(context);
                    },
                  ),
                if (!member.isIncluded)
                  ListTile(
                    leading: const Icon(Icons.keyboard_return),
                    title: (member.isPrioritized)
                        ? const Text('Cancel prioritization')
                        : const Text('Return'),
                    onTap: () {
                      emitSetState(AppMemberState.included);
                      Navigator.pop(context);
                    },
                  ),
                if (member.isIncluded || member.isPrioritized)
                  ListTile(
                    leading: const Icon(Icons.close),
                    title: const Text('Leave'),
                    onTap: () {
                      emitSetState(AppMemberState.excluded);
                      Navigator.pop(context);
                    },
                  ),
                if (!member.isExcluded)
                  ListTile(
                    leading: const Icon(Icons.arrow_downward),
                    title: const Text('Shift down'),
                    onTap: () {
                      queue.memberShift(member, 1);

                      context.read<AppState>().socket.emit('memberShift', {
                        'group': group,
                        'queue': queue.id,
                        'queueMember': member.id,
                        'shift': 1
                      });
                      Navigator.pop(context);
                    },
                  ),
                if (member.isIncluded)
                  ListTile(
                    leading: const Icon(Icons.emergency),
                    title: const Text('Priority request'),
                    onTap: () {
                      emitSetState(AppMemberState.prioritized);
                      Navigator.pop(context);
                    },
                  ),
                ListTile(
                  leading: Icon(
                    Icons.delete,
                    color: Theme.of(context).colorScheme.error,
                  ),
                  title: const Text('Erase'),
                  onTap: () {
                    Navigator.pop(context);
                    showDialog(
                      context: context,
                      builder: (context) => AlertDialog(
                        title: const Text('Erase record'),
                        content: const SingleChildScrollView(
                          child: Text(
                            '''Are you sure you want to erase this record forever?
If you want to leave the queue, prefer the "leave" option.''',
                          ),
                        ),
                        actions: [
                          TextButton(
                            style: TextButton.styleFrom(
                                foregroundColor:
                                    Theme.of(context).colorScheme.error),
                            onPressed: () {
                              context.read<AppState>().socket.emit(
                                  'memberErase', {
                                'group': group,
                                'queue': queue.id,
                                'queueMember': member.id
                              });
                              Navigator.pop(context);
                            },
                            child: const Text(
                              'Erase',
                              textScaler: TextScaler.linear(1.25),
                            ),
                          ),
                          TextButton(
                            onPressed: () {
                              Navigator.pop(context);
                            },
                            child: const Text(
                              'Cancel',
                              textScaler: TextScaler.linear(1.25),
                            ),
                          ),
                        ],
                      ),
                    );
                  },
                ),
              ],
            ),
          ),
        ),
      );
    }

    return builder(context, onTap);
  }
}

class MemberTile extends StatelessWidget {
  final BuildContext context;
  final AppQueueMember? member;
  final AppQueue queue;
  final bool showShift;

  const MemberTile({
    super.key,
    required this.context,
    this.member,
    required this.queue,
    this.showShift = true,
  });

  @override
  Widget build(BuildContext context) {
    if (member == null) return const ListTile(title: Text('No member'));

    return ChangeNotifierProvider.value(
      value: queue,
      child: ChangeNotifierProvider.value(
        value: member,
        child: Consumer<AppQueueMember>(
          builder: (context, member, child) {
            final index = context
                .select<AppQueue, List<AppQueueMember>>(
                    (queue) => queue.sequence)
                .indexOf(member);
            final opacity = (member.isExcluded || member.isSkipped) ? 0.5 : 1.0;
            final primaryColor = (member.isPrioritized)
                ? Theme.of(context).colorScheme.error
                : Theme.of(context).colorScheme.onSurfaceVariant;
            final textStyle = (member.isExcluded)
                ? const TextStyle(decoration: TextDecoration.lineThrough)
                : const TextStyle();

            return Opacity(
              opacity: opacity,
              child: MemberTileTapHandler(
                member: member,
                queue: queue,
                builder: (context, onTap) => ListTile(
                  onTap: onTap,
                  iconColor: primaryColor,
                  leading: Text(
                    '#${index + 1}',
                    style: textStyle.copyWith(color: primaryColor),
                    textScaler: const TextScaler.linear(1.5),
                  ),
                  title: Row(
                    children: [
                      const Icon(Icons.person),
                      Text(
                        member.user.name,
                        style: textStyle,
                      ),
                    ],
                  ),
                  trailing: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      // it's for debugging now, but maybe I can keep this
                      if (showShift)
                        Row(
                          children: [
                            const Icon(
                              Icons.arrow_downward,
                              size: 16.0,
                            ),
                            Text(
                              member.shift.toString(),
                              style: textStyle.copyWith(color: primaryColor),
                            ),
                            const SizedBox(
                              width: 16.0,
                            )
                          ],
                        ),
                    ],
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
