import 'package:bson/bson.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:queuer/models/app_group.dart';
import 'package:queuer/models/app_state.dart';
import 'package:queuer/screens/create_group_screen.dart';
import 'package:queuer/screens/group_screen.dart';
import 'package:queuer/screens/settings_screen.dart';
import 'package:queuer/screens/sign_in_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Groups'),
        actions: [
          MenuAnchor(
            menuChildren: [
              MenuItemButton(
                  leadingIcon: const Icon(Icons.search),
                  onPressed: () {
                    showDialog(
                      context: context,
                      builder: (context) => const SearchGroupDialog(),
                    );
                  },
                  child: const Text('Search group')),
              MenuItemButton(
                leadingIcon: const Icon(Icons.settings),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => const SettingsScreen(),
                    ),
                  );
                },
                child: const Text('Settings'),
              ),
              MenuItemButton(
                style: MenuItemButton.styleFrom(
                  foregroundColor: Theme.of(context).colorScheme.error,
                  iconColor: Theme.of(context).colorScheme.error,
                ),
                leadingIcon: const Icon(Icons.logout),
                onPressed: () {
                  SharedPreferences.getInstance()
                      .then((value) => value.setBool('isLoggedIn', false));

                  context.read<AppState>().disposeSocket();

                  Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                      builder: (context) => const SignInScreen(),
                    ),
                  );
                },
                child: const Text('Log out'),
              ),
            ],
            builder: (context, controller, child) => IconButton(
              icon: const Icon(Icons.more_vert),
              onPressed: () {
                if (controller.isOpen) {
                  controller.close();
                } else {
                  controller.open();
                }
              },
            ),
          ),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Consumer<AppState>(
          builder: (context, state, child) => ListView(
            children: [
              if (state.groups.isEmpty)
                const Padding(
                  padding: EdgeInsets.all(24.0),
                  child: Text(
                    'There are no groups yet',
                    textAlign: TextAlign.center,
                  ),
                ),
              for (var group in state.groups)
                ChangeNotifierProvider.value(
                  value: group,
                  builder: (context, child) => Consumer<AppGroup>(
                    builder: (context, group, child) => ListTile(
                      leading: const Icon(Icons.star),
                      title: Text(group.name),
                      onTap: () {
                        state.currentGroup = group;
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => const GroupScreen()));
                      },
                    ),
                  ),
                ),
            ],
          ),
        ),
      ),
      floatingActionButton: ElevatedButton(
        onPressed: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => const CreateGroupScreen()));
        },
        child: const Padding(
          padding: EdgeInsets.symmetric(vertical: 8.0),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              Icon(Icons.add),
              Text('Create'),
            ],
          ),
        ),
      ),
    );
  }
}

class SearchGroupDialog extends StatefulWidget {
  const SearchGroupDialog({super.key});

  @override
  State<SearchGroupDialog> createState() => _SearchGroupDialogState();
}

class _SearchGroupDialogState extends State<SearchGroupDialog> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController _idController = TextEditingController();
  final linkStart = 'queuer.com/g/';

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    _idController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: const Text('Search group'),
      content: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: TextFormField(
            decoration: const InputDecoration(
              label: Text('Group link'),
              contentPadding: EdgeInsets.all(8.0),
            ),
            controller: _idController,
            validator: (value) {
              if (value == null ||
                  value.length != linkStart.length + 24 ||
                  !ObjectId.isValidHexId(
                      value.trim().substring(linkStart.length))) {
                return 'Please enter the correct link';
              }
              return null;
            },
          ),
        ),
      ),
      actions: [
        TextButton(
          onPressed: () {
            final hex = _idController.text.trim().substring(linkStart.length);
            if (!_formKey.currentState!.validate()) return;

            context
                .read<AppState>()
                .socket
                .emit('addGroupMember', {'group': hex});
            Navigator.pop(context);
          },
          child: const Text(
            'Search',
            textScaler: TextScaler.linear(1.25),
          ),
        ),
        TextButton(
          onPressed: () {
            Navigator.pop(context);
          },
          child: const Text(
            'Cancel',
            textScaler: TextScaler.linear(1.25),
          ),
        ),
      ],
    );
  }
}
