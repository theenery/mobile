import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:queuer/models/app_group.dart';
import 'package:queuer/models/app_mixed_queue.dart';
import 'package:queuer/models/app_mixed_queue_member.dart';
import 'package:queuer/models/app_queue_member.dart';
import 'package:queuer/screens/group_settings_screen.dart';
import 'package:queuer/screens/queue_screen.dart';

class MixedQueueScreen extends StatelessWidget {
  const MixedQueueScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final mixedQueue =
        context.select<AppGroup, AppMixedQueue>((group) => group.mixedQueue);

    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          title: Selector<AppGroup, String>(
            selector: (context, group) => group.name,
            builder: (context, name, child) => Text(name),
          ),
          actions: [
            IconButton(
              icon: const Icon(Icons.settings),
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const GroupSettingsScreen()));
              },
            )
          ],
          bottom: TabBar(
            unselectedLabelColor: Theme.of(context).colorScheme.primary,
            tabs: const [Tab(text: 'All'), Tab(text: 'Brief')],
            indicatorSize: TabBarIndicatorSize.tab,
            labelPadding: const EdgeInsets.all(0.0),
          ),
        ),
        body: ChangeNotifierProvider.value(
          value: mixedQueue,
          child: TabBarView(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Consumer<AppMixedQueue>(
                  builder: (context, mixedQueue, child) {
                    if (mixedQueue.sequence.isEmpty) {
                      return const Padding(
                        padding: EdgeInsets.all(24.0),
                        child: Text(
                          'There are no members yet',
                          textAlign: TextAlign.center,
                        ),
                      );
                    } else {
                      return ListView.builder(
                        itemCount: mixedQueue.sequence.length,
                        itemBuilder: (context, index) => MixedMemberTile(
                          context: context,
                          mixedMember: mixedQueue.sequence[index],
                          mixedQueue: mixedQueue,
                          showShift: false,
                        ),
                      );
                    }
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: Consumer<AppMixedQueue>(
                  builder: (context, mixedQueue, child) => Column(
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text(
                            'Previous',
                            textScaler: TextScaler.linear(1.25),
                          ),
                          MixedMemberTile(
                            context: context,
                            mixedMember: mixedQueue.previousMember,
                            mixedQueue: mixedQueue,
                            showShift: false,
                          )
                        ],
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text(
                            'Current',
                            textScaler: TextScaler.linear(1.25),
                          ),
                          MixedMemberTile(
                            context: context,
                            mixedMember: mixedQueue.currentMember,
                            mixedQueue: mixedQueue,
                            showShift: false,
                          ),
                        ],
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text(
                            'Next',
                            textScaler: TextScaler.linear(1.25),
                          ),
                          MixedMemberTile(
                            context: context,
                            mixedMember: mixedQueue.nextMember,
                            mixedQueue: mixedQueue,
                            showShift: false,
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          Text(
                            'Remains ${mixedQueue.membersRemains}',
                            textScaler: const TextScaler.linear(1.25),
                          ),
                          Icon(
                            Icons.person,
                            color: Theme.of(context).colorScheme.surfaceVariant,
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class MixedMemberTile extends StatelessWidget {
  final BuildContext context;
  final AppMixedQueueMember? mixedMember;
  final AppMixedQueue mixedQueue;
  final bool showShift;

  const MixedMemberTile({
    super.key,
    required this.context,
    this.mixedMember,
    required this.mixedQueue,
    this.showShift = true,
  });

  @override
  Widget build(BuildContext context) {
    if (mixedMember == null) return const ListTile(title: Text('No member'));

    return ChangeNotifierProvider.value(
      value: mixedMember!.member,
      child: Consumer<AppQueueMember>(
        builder: (context, member, child) {
          final index = mixedQueue.sequence.indexOf(mixedMember!);
          final opacity = (member.isExcluded || member.isSkipped) ? 0.5 : 1.0;
          final textStyle = (member.isExcluded)
              ? const TextStyle(decoration: TextDecoration.lineThrough)
              : const TextStyle();
          final primaryColor = (member.isPrioritized)
              ? Theme.of(context).colorScheme.error
              : Theme.of(context).colorScheme.onSurfaceVariant;
          return Opacity(
            opacity: opacity,
            child: MemberTileTapHandler(
              member: member,
              queue: mixedMember!.queue,
              builder: (context, onTap) => ListTile(
                onTap: onTap,
                subtitle: Row(
                  children: [
                    const Icon(Icons.subdirectory_arrow_right, size: 20.0),
                    const Icon(Icons.group, size: 20.0),
                    Text(mixedMember!.queue.name,
                        style: textStyle.copyWith(color: primaryColor)),
                  ],
                ),
                iconColor: primaryColor,
                leading: Text(
                  '#${index + 1}',
                  style: textStyle.copyWith(color: primaryColor),
                  textScaler: const TextScaler.linear(1.5),
                ),
                title: Row(
                  children: [
                    const Icon(Icons.person),
                    Text(
                      member.user.name,
                      style: textStyle,
                    ),
                  ],
                ),
                trailing: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    // it's for debugging now, but maybe I can keep this
                    if (showShift)
                      Row(
                        children: [
                          const Icon(
                            Icons.arrow_downward,
                            size: 16.0,
                          ),
                          Text(
                            member.shift.toString(),
                            style: textStyle.copyWith(color: primaryColor),
                          ),
                        ],
                      ),
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
