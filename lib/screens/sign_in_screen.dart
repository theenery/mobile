import 'package:bson/bson.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:queuer/models/app_state.dart';
import 'package:queuer/screens/home_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SignInScreen extends StatefulWidget {
  const SignInScreen({super.key});

  @override
  State<SignInScreen> createState() => _SignInScreenState();
}

class _SignInScreenState extends State<SignInScreen> {
  final _formKey = GlobalKey<FormState>();
  late TextEditingController _idController = TextEditingController(text: '');

  @override
  void initState() {
    super.initState();

    SharedPreferences.getInstance().then((value) {
      final id = value.getString('login') ?? '65f4d0a01312b7c8b6e4fa7d';
      setState(() {
        _idController = TextEditingController(text: id);
      });
    });
  }

  @override
  void dispose() {
    _idController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SharedPreferences.getInstance().then(
      (value) {
        if (value.getBool('isLoggedIn') ?? false) signIn(context);
      },
    );

    return Scaffold(
      body: Form(
        key: _formKey,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 24.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              // Pseudo-authorization for debug
              const Text(
                'Sign in',
                textScaler: TextScaler.linear(3.0),
              ),
              const SizedBox(height: 24.0),
              TextFormField(
                decoration: const InputDecoration(
                  label: Text('Your id'),
                  contentPadding: EdgeInsets.all(8.0),
                ),
                controller: _idController,
                validator: (value) {
                  if (value == null || !ObjectId.isValidHexId(value.trim())) {
                    return 'Please enter the correct id';
                  }
                  return null;
                },
              ),
            ],
          ),
        ),
      ),
      floatingActionButton: ElevatedButton(
        onPressed: () {
          signIn(context);
        },
        child: const Padding(
          padding: EdgeInsets.symmetric(vertical: 8.0),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text('Continue'),
              Icon(Icons.arrow_forward),
            ],
          ),
        ),
      ),
    );
  }

  void signIn(BuildContext context) {
    final hex = _idController.text.trim();
    if (!_formKey.currentState!.validate()) return;
    SharedPreferences.getInstance().then((value) {
      value.setString('login', hex);
      value.setBool('isLoggedIn', true);
    });

    context.read<AppState>().user.id = ObjectId.fromHexString(hex);
    context.read<AppState>().initSocket();

    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (context) => const HomeScreen()));
  }
}
