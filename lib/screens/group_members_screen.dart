import 'package:bson/bson.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:queuer/models/app_group.dart';
import 'package:queuer/models/app_group_member.dart';
import 'package:queuer/models/app_state.dart';
import 'package:queuer/models/app_user.dart';

class GroupMembersScreen extends StatelessWidget {
  const GroupMembersScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final user = context.select<AppUser, ObjectId>((user) => user.id);

    return Scaffold(
      appBar: AppBar(
        title: const Text('Members'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Consumer<AppGroup>(
          builder: (context, group, child) {
            bool userIsOwner = user == group.owner;
            return ListView.builder(
              itemCount: group.members.length,
              itemBuilder: (context, index) => ChangeNotifierProvider.value(
                value: group.members[index],
                child: Consumer<AppGroupMember>(
                  builder: (context, member, child) {
                    bool userIsMember = member.user.id == user;
                    String? subtitle;
                    if (member.isModerator) subtitle = 'Moderator';
                    if (member.user.id == group.owner) subtitle = 'Owner';

                    return ListTile(
                      contentPadding:
                          const EdgeInsets.symmetric(horizontal: 8.0),
                      title: Text(member.user.name),
                      subtitle: (subtitle != null) ? Text(subtitle) : null,
                      leading: const Icon(Icons.person),
                      trailing: (userIsOwner && !userIsMember)
                          ? MenuAnchor(
                              menuChildren: [
                                if (!member.isModerator)
                                  MenuItemButton(
                                      leadingIcon:
                                          const Icon(Icons.add_moderator),
                                      onPressed: () {
                                        context
                                            .read<AppState>()
                                            .socket
                                            .emit('updateModerator', {
                                          'group': group.id,
                                          'groupMember': member.user.id,
                                          'isModerator': true,
                                        });
                                      },
                                      child:
                                          const Text('Grant moderator rights')),
                                if (member.isModerator)
                                  MenuItemButton(
                                    leadingIcon:
                                        const Icon(Icons.remove_moderator),
                                    onPressed: () {
                                      context
                                          .read<AppState>()
                                          .socket
                                          .emit('updateModerator', {
                                        'group': group.id,
                                        'groupMember': member.user.id,
                                        'isModerator': false,
                                      });
                                    },
                                    child:
                                        const Text('Revoke moderator rights'),
                                  ),
                                MenuItemButton(
                                  style: MenuItemButton.styleFrom(
                                    foregroundColor:
                                        Theme.of(context).colorScheme.error,
                                    iconColor:
                                        Theme.of(context).colorScheme.error,
                                  ),
                                  leadingIcon: const Icon(Icons.person_off),
                                  onPressed: () {
                                    context
                                        .read<AppState>()
                                        .socket
                                        .emit('deleteGroupMember', {
                                      'group': group.id,
                                      'groupMember': member.user.id,
                                    });
                                  },
                                  child: const Text('Remove member'),
                                ),
                              ],
                              builder: (context, controller, child) =>
                                  IconButton(
                                icon: const Icon(Icons.more_vert),
                                onPressed: () {
                                  if (controller.isOpen) {
                                    controller.close();
                                  } else {
                                    controller.open();
                                  }
                                },
                              ),
                            )
                          : null,
                    );
                  },
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
