import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:queuer/models/app_group.dart';
import 'package:queuer/models/app_queue.dart';
import 'package:queuer/models/app_state.dart';
import 'package:queuer/models/app_user.dart';
import 'package:queuer/screens/sign_in_screen.dart';
import 'package:queuer/themes/app_color_sheme.dart';

// very strange tricks
void main() {
  runApp(
    ChangeNotifierProvider(
      create: (context) => AppState(),
      child: Selector<AppState, AppGroup>(
        selector: (context, state) => state.currentGroup,
        builder: (context, group, child) {
          return ChangeNotifierProvider<AppGroup>.value(
            value: group,
            child: child,
          );
        },
        child: Selector<AppState, AppQueue>(
          selector: (context, state) => state.currentQueue,
          builder: (context, queue, child) {
            return ChangeNotifierProvider<AppQueue>.value(
              value: queue,
              child: child,
            );
          },
          child: Selector<AppState, AppUser>(
            selector: (context, state) => state.user,
            builder: (context, user, child) {
              return ChangeNotifierProvider<AppUser>.value(
                value: user,
                child: child,
              );
            },
            child: ChangeNotifierProvider(
              create: (context) => AppColorScheme.fromPreferences(),
              child: const App(),
            ),
          ),
        ),
      ),
    ),
  );
}

class App extends StatelessWidget {
  const App({super.key});

  themeOf(ColorScheme colorScheme) => ThemeData(
        useMaterial3: true,
        iconTheme: const IconThemeData(size: 32.0),
        colorScheme: colorScheme,
        appBarTheme: const AppBarTheme(
          centerTitle: true,
          actionsIconTheme: IconThemeData(size: 32.0),
        ),
        bottomSheetTheme:
            BottomSheetThemeData(backgroundColor: colorScheme.background),
        menuTheme: MenuThemeData(
          style: MenuStyle(
            backgroundColor: MaterialStatePropertyAll(colorScheme.background),
          ),
        ),
      );

  @override
  Widget build(BuildContext context) {
    final colorScheme = context.watch<AppColorScheme>();

    return MaterialApp(
      title: 'Queuer',
      themeMode: colorScheme.themeMode,
      theme: themeOf(colorScheme.lightScheme),
      darkTheme: themeOf(colorScheme.darkScheme),
      home: const SignInScreen(),
    );
  }
}
