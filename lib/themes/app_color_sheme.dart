// ignore_for_file: unused_field

import 'package:flutter/material.dart';
import 'package:queuer/themes/app_color_palette.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AppColorScheme with ChangeNotifier {
  static const dark = Color(0xFF272433);
  static const light = Color(0xFFF7FAFA);
  static const errorDark = Color(0xFFAA1111);
  static const errorLight = Color(0xFFFC94A5);

  static const _debug = Colors.red;
  static const _debugAlt = Colors.cyan;
  static const _hide = Colors.transparent;

  late AppColorPalette _colorPalette;
  late ColorScheme _darkScheme;
  late ColorScheme _lightScheme;
  late ThemeMode _themeMode;

  // this constructor doesn`t update preferences
  AppColorScheme({AppColorPalette? colorPalette, ThemeMode? themeMode})
      : _colorPalette = colorPalette ?? AppColorPalette.palettes[0],
        _themeMode = themeMode ?? ThemeMode.system {
    _updateSchemes();
  }

  factory AppColorScheme.fromPreferences() {
    final colorScheme = AppColorScheme();
    SharedPreferences.getInstance().then((value) {
      colorScheme.colorPalette =
          AppColorPalette.palettes[value.getInt('colorPalette') ?? 0];
      colorScheme.themeMode = ThemeMode.values
          .byName(value.getString('themeMode') ?? ThemeMode.system.name);
    });
    return colorScheme;
  }

  AppColorPalette get colorPalette => _colorPalette;
  ColorScheme get darkScheme => _darkScheme;
  ColorScheme get lightScheme => _lightScheme;
  ThemeMode get themeMode => _themeMode;

  set colorPalette(AppColorPalette palette) {
    _colorPalette = palette;
    SharedPreferences.getInstance().then((value) => value.setInt(
        'colorPalette', AppColorPalette.palettes.indexOf(palette)));
    _updateSchemes();
  }

  set themeMode(ThemeMode themeMode) {
    _themeMode = themeMode;
    SharedPreferences.getInstance()
        .then((value) => value.setString('themeMode', themeMode.name));
    notifyListeners();
  }

  ColorScheme _colorSchemeOf(
    Color primary,
    Color secondary,
    Color accent,
    Color accentVariant,
    Color error,
    Brightness brightness,
  ) =>
      ColorScheme(
        brightness: brightness,
        primary: secondary,
        onPrimary: _debug,
        primaryContainer: _debug,
        onPrimaryContainer: _debug,
        secondary: _debug,
        onSecondary: _debug,
        secondaryContainer: _debug,
        onSecondaryContainer: _debug,
        tertiary: _debug,
        onTertiary: _debug,
        tertiaryContainer: _debug,
        onTertiaryContainer: _debug,
        error: error,
        onError: _debug,
        errorContainer: _debug,
        onErrorContainer: secondary,
        background: primary,
        onBackground: _debug,
        surface: accent,
        onSurface: secondary,
        surfaceVariant: accentVariant,
        onSurfaceVariant: accentVariant,
        outline: accent,
        outlineVariant: _debug,
        shadow: accent,
        scrim: _debug,
        inverseSurface: accent,
        onInverseSurface: secondary,
        inversePrimary: _debug,
        surfaceTint: accent,
      );

  void _updateSchemes() {
    _darkScheme = _colorSchemeOf(
      dark,
      light,
      _colorPalette.accent,
      _colorPalette.accentVariant,
      errorLight,
      Brightness.dark,
    );
    _lightScheme = _colorSchemeOf(
      light,
      dark,
      _colorPalette.accentVariant,
      _colorPalette.accent,
      errorDark,
      Brightness.light,
    );

    notifyListeners();
  }
}
