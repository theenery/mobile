import 'dart:ui';

class AppColorPalette {
  final Color accent;
  final Color accentVariant;

  const AppColorPalette({required this.accent, required this.accentVariant});

  static const palettes = [
    AppColorPalette(
      accent: Color(0xFF1C5E59),
      accentVariant: Color(0xFF3FC6B0),
    ),
    AppColorPalette(
      accent: Color(0xFF2B4EAD),
      accentVariant: Color(0xFF60BBF5),
    ),
    AppColorPalette(
      accent: Color(0xFF822AA1),
      accentVariant: Color(0xFFDF96FD),
    ),
    AppColorPalette(
      accent: Color(0xFF605501),
      accentVariant: Color(0xFFB9B800),
    ),
    AppColorPalette(
      accent: Color(0xFFA30968),
      accentVariant: Color(0xFFF890D3),
    ),
    AppColorPalette(
      accent: Color(0xFF186214),
      accentVariant: Color(0xFF36CD2A),
    ),
  ];
}
